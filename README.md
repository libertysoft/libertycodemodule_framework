LibertyCodeModule_Framework
===========================



Description
-----------

Library contains framework modules, 
to manage application framework engine.

---



Requirement
-----------

- Script language: PHP: version ~7 || ~8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code_module/framework ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code_module/framework": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Application installation
------------------------

#### Configuration

1. Configuration: application module: "<project root path>/config/Module.<config_file_ext>"

    Add in list part, required modules:

    Example for YML configuration format, from composer installation:

    ```yml
    list: [
        {
            path: "/vendor/liberty_code_module/framework/src/application",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/framework/src/app_mode",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/framework/src/cache",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/framework/src/module",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        }
    ]
    ```

---



Usage
-----

Following examples consider command line application relative path as 
"bin/app".

#### Application

Features allow to get framework application information.

_Example_

```sh
cd "<project_root_path>"
...
// Get application root directory full path
php bin/app app:get:root_path
```

#### Application mode

Features allow to get framework application modes information.

_Example_

```sh
cd "<project_root_path>"
...
// Get all application mode keys
php bin/app app_mode:get:key -a
```

#### Cache

Features allow to manage framework application cache.

_Example_

```sh
cd "<project_root_path>"
...
// Remove all items, from cache
php bin/app cache:remove
```

#### Module

Features allow to manage modules, in framework application.

_Example_

```sh
cd "<project_root_path>"
...
// Create specified module
// (module_root_path is relative, from application root path)
php bin/app module:create "<module_key>" "<module_root_path>"
...
// Register specified module, in application
php bin/app module:register "<module_root_path>"
...
// Get all module keys
php bin/app module:get:key "<module_root_path>"
```

---


