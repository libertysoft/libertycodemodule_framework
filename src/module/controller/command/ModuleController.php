<?php
/**
 * Description :
 * This class allows to define module controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\module\controller\command;

use liberty_code_module\framework\controller\DefaultController;

use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\parser\file\factory\api\FileParserFactoryInterface;
use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\parser\build\factory\model\FactoryBuilder as ParserBuilder;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\request_flow\response\info\library\ToolBoxInfoResponse;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code\framework\application\api\AppInterface;
use liberty_code_module\framework\module\library\ConstModule;
use liberty_code_module\framework\module\library\ToolBoxModule;
use liberty_code_module\framework\module\exception\KeyNotFoundException;



class ModuleController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Parser factory
     * @var ParserFactoryInterface
     */
    protected $objParserFactory;



    /**
     * File parser factory
     * @var FileParserFactoryInterface
     */
    protected $objFileParserFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AppInterface $objApp
     * @param ParserFactoryInterface $objParserFactory
     * @param FileParserFactoryInterface $objFileParserFactory
     */
    public function __construct(
        AppInterface $objApp,
        ParserFactoryInterface $objParserFactory,
        FileParserFactoryInterface $objFileParserFactory
    )
    {
        // Init properties
        $this->objParserFactory = $objParserFactory;
        $this->objFileParserFactory = $objFileParserFactory;

        // Call parent constructor
        parent::__construct($objApp);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get parser builder object, for configuration,
     * from command line option.
     *
     * @param CommandFrontController $objFrontController
     * @return null|ParserBuilderInterface
     */
    protected function getObjConfigParserBuilder(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strConfig = $objFrontController->getOptValue(
            ConstModule::COMMAND_OPT_NAME_CONFIG,
            null
        );
        $result = null;

        // Get config parser builder, if required
        if(!is_null($strConfig))
        {
            // Get configuration
            $tabConfig = json_decode($strConfig, true);
            if(!is_array($tabConfig))
            {
                $tabConfig = array(
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => $strConfig
                );
            }

            // Get parser builder
            $result = new ParserBuilder(
                $this->objParserFactory,
                $this->objFileParserFactory,
                $tabConfig
            );
        }

        // Return result
        return $result;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Action to get module keys.
     *
     * @return DefaultResponse
     */
    public function actionGetKey()
    {
        // Init var
        $objApp = $this->objApp;

        // Get render
        $strRender = implode(
            PHP_EOL,
            $objApp
                ->getObjModuleCollection()
                ->getTabKey()
        );

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Action to get module.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     * @throws KeyNotFoundException
     */
    public function actionGet(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getArgValue(ConstModule::COMMAND_ARG_NAME_KEY, null);
        $objApp = $this->objApp;
        $strFormat =
            'Module "%1$s" (path: "%2$s")' . PHP_EOL .
            PHP_EOL .
            'Configuration type: %3$s' . PHP_EOL .
             PHP_EOL .
            'Configuration:' . PHP_EOL .
            '%4$s';

        // Get module
        $objModule = $objApp
            ->getObjModuleCollection()
            ->getObjModule($strKey);
        if(is_null($objModule))
        {
            throw new KeyNotFoundException($strKey);
        }

        // Get configuration parser builder
        $objConfigParserBuilder = (
            (!is_null($configParserBuilder = $objModule->getObjConfigParserBuilder())) ?
                $configParserBuilder :
                $objApp->getObjConfigParserBuilder()
        );

        // Get render
        $strRender = sprintf(
            $strFormat,
            $objModule->getStrKey(),
            $objModule->getStrRootDirPath(),
            get_class($objConfigParserBuilder->getObjParser()),
            ToolBoxInfoResponse::getStrTable(array(
                ['parameter', $objModule->checkConfigExists()],
                ['boot', $objModule->checkBootExists()],
                ['application parameter', (!is_null($objModule->getTabConfigParamApp()))],
                ['autoload', (!is_null($objModule->getTabConfigAutoload()))],
                ['dependency injection', (!is_null($objModule->getTabConfigDi()))],
                ['register', (!is_null($objModule->getTabConfigRegister()))],
                ['event', (!is_null($objModule->getTabConfigEvent()))],
                ['web route', (!is_null($objModule->getTabConfigRouteWeb()))],
                ['command line route', (!is_null($objModule->getTabConfigRouteCmd()))]
            ))
        );

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Action to create module.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionCreate(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getArgValue(ConstModule::COMMAND_ARG_NAME_KEY);
        $strRootDirPath = $objFrontController->getArgValue(ConstModule::COMMAND_ARG_NAME_ROOT_DIR_PATH);
        $boolConfigTemplate = ($objFrontController->getOptValue(ConstModule::COMMAND_OPT_NAME_CONFIG_TEMPLATE, false) !== false);
        $boolRegister = ($objFrontController->getOptValue(ConstModule::COMMAND_OPT_NAME_REGISTER, false) !== false);
        $objConfigParserBuilder = $this->getObjConfigParserBuilder($objFrontController);
        $objApp = $this->objApp;

        // Create module
        ToolBoxModule::create(
            $objApp,
            $strKey,
            $strRootDirPath,
            $objConfigParserBuilder,
            $boolConfigTemplate
        );

        // Register module, if required
        if($boolRegister)
        {
            ToolBoxModule::register(
                $objApp,
                $strRootDirPath,
                $objConfigParserBuilder,
                true
            );
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Action to register modules.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRegister(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strRootDirPath = $objFrontController->getArgValue(ConstModule::COMMAND_ARG_NAME_ROOT_DIR_PATH);
        $boolRegister = ($objFrontController->getOptValue(ConstModule::COMMAND_OPT_NAME_UNREGISTER, false) === false);
        $boolList = ($objFrontController->getOptValue(ConstModule::COMMAND_OPT_NAME_LIST, false) !== false);
        $objConfigParserBuilder = $this->getObjConfigParserBuilder($objFrontController);
        $objApp = $this->objApp;

        // Register list of modules
        if($boolList)
        {
            ToolBoxModule::registerMulti(
                $objApp,
                $strRootDirPath,
                $objConfigParserBuilder,
                $boolRegister
            );
        }
        // Register module
        else
        {
            ToolBoxModule::register(
                $objApp,
                $strRootDirPath,
                $objConfigParserBuilder,
                $boolRegister
            );
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



}