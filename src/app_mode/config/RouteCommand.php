<?php

use liberty_code_module\framework\app_mode\library\ConstAppMode;
use liberty_code_module\framework\app_mode\controller\command\AppModeController;



return array(
    // Application mode routes
    // ******************************************************************************

    'app_mode_get_key' => [
        'source' => 'app_mode:get:key',
        'call' => [
            'class_path_pattern' => AppModeController::class . ':actionGetKey'
        ],
        'description' => 'Get active or all application mode keys',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstAppMode::COMMAND_OPT_NAME_ALL, 'a'],
                'description' => 'Require all keys',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ],

    'app_mode_get' => [
        'source' => 'app_mode:get',
        'call' => [
            'class_path_pattern' => AppModeController::class . ':actionGet'
        ],
        'description' => 'Get active or specified application mode',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstAppMode::COMMAND_OPT_NAME_KEY, 'k'],
                'description' => 'Specified application mode key, to get',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ]
        ]
    ]
);