<?php
/**
 * Description :
 * This class allows to define application controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\application\controller\command;

use liberty_code_module\framework\controller\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;



class AppController extends DefaultController
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods action
    // ******************************************************************************

    /**
     * Action to get application root directory full path.
     *
     * @return DefaultResponse
     */
    public function actionGetRootDirPath()
    {
        // Init var
        $objApp = $this->objApp;

        // Get render
        $strRender = $objApp->getStrRootDirPath();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



}