<?php
/**
 * Description :
 * This class allows to define AppMode controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\app_mode\controller\command;

use liberty_code_module\framework\controller\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\request_flow\response\info\library\ToolBoxInfoResponse;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code_module\framework\app_mode\library\ConstAppMode;
use liberty_code_module\framework\app_mode\exception\KeyNotFoundException;



class AppModeController extends DefaultController
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods action
    // ******************************************************************************

    /**
     * Action to get application mode keys.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionGetKey(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $boolAll = ($objFrontController->getOptValue(ConstAppMode::COMMAND_OPT_NAME_ALL, false) !== false);
        $objApp = $this->objApp;

        // Get render
        $strRender = (
            $boolAll ?
                // Get all keys
                implode(
                    PHP_EOL,
                    $objApp
                        ->getObjAppMode()
                        ->getObjModeCollection()
                        ->getTabKey()
                ) :
                // Get active key
                $objApp
                    ->getObjAppMode()
                    ->getObjActiveMode()
                    ->getStrKey()
        );

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Action to get application mode.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     * @throws KeyNotFoundException
     */
    public function actionGet(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getOptValue(ConstAppMode::COMMAND_OPT_NAME_KEY, false);
        $objApp = $this->objApp;
        $strFormatMode = 'Mode "%1$s"';
        $strFormatModeConfig = 'Configuration mode "%1$s": ' . PHP_EOL . '%2$s';

        // Get mode
        $objMode = (
            ($strKey !== false) ?
                $objApp->getObjAppMode()->getObjModeCollection()->getObjMode($strKey) :
                $objApp->getObjAppMode()->getObjActiveMode()
        );
        if(is_null($objMode))
        {
            throw new KeyNotFoundException($strKey);
        }

        // Get configuration data
        $tabData = array();
        foreach($objMode->getObjConfig()->getTabKey() as $strKey)
        {
            $tabData[] = array(
                $strKey,
                $objMode->getObjConfig()->getValue($strKey)
            );
        }
        $strData = ToolBoxInfoResponse::getStrTable($tabData);

        // Get render
        $strRender = sprintf($strFormatMode, $objMode->getStrKey());
        if(!is_null($strData))
        {
            $strRender = sprintf($strFormatModeConfig, $objMode->getStrKey(), $strData);
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



}