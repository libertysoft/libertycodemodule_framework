<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\module\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\framework\module\library\ConstModule as BaseConstModule;
use liberty_code\framework\module\exception\KeyInvalidFormatException;
use liberty_code\framework\module\exception\RootDirPathInvalidFormatException;
use liberty_code\framework\module\factory\standard\library\ConstStandardModuleFactory;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\module\build\library\ConstBuilder as ConstModuleBuilder;
use liberty_code_module\framework\module\exception\RootDirPathAddInvalidFormatException;
use liberty_code_module\framework\module\exception\RootDirPathMultiInvalidFormatException;



class ToolBoxModule extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Create specified module.
     *
     * @param AppInterface $objApp
     * @param string $strKey
     * @param string $strRootDirPath
	 * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @param boolean $boolConfigTemplate = false
     * @throws KeyInvalidFormatException
     * @throws RootDirPathAddInvalidFormatException
     */
    public static function create(
        AppInterface $objApp,
        $strKey,
        $strRootDirPath,
        ParserBuilderInterface $objConfigParserBuilder = null,
        $boolConfigTemplate = false
    )
    {
        // Init var
        $strRootDirPath = $objApp->getStrRootDirPath() . $strRootDirPath;
        $objConfigParserBuilder = (is_null($objConfigParserBuilder) ? $objApp->getObjConfigParserBuilder() : $objConfigParserBuilder);

        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);
        RootDirPathAddInvalidFormatException::setCheck($strRootDirPath);

        // Get config parser
        $objFileParser = $objConfigParserBuilder->getObjFileParser();

        // Create module directories
        mkdir($strRootDirPath);
        mkdir($strRootDirPath . BaseConstModule::CONF_PATH_DIR_CONFIG);

        // Create file config module
        $objFileParser->setSource(
            $strRootDirPath . BaseConstModule::CONF_PATH_FILE_CONFIG_MODULE . $objFileParser->getStrFileExt(),
            array(
                BaseConstModule::TAB_CONFIG_MODULE_KEY_KEY => $strKey
            )
        );

        // Create configuration template, if required
        if(is_bool($boolConfigTemplate) && $boolConfigTemplate)
        {
            $tabConfig = array(
                [BaseConstModule::CONF_PATH_FILE_CONFIG_PARAM, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_BOOT, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_PARAM_APP, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_AUTOLOAD, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_DI, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_REGISTER, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_EVENT, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_ROUTE_WEB, array('template')],
                [BaseConstModule::CONF_PATH_FILE_CONFIG_ROUTE_COMMAND, array('template')]
            );

            // Run all configs
            foreach($tabConfig as $tabConfigInfo)
            {
                // Get info
                $strPathFile = $tabConfigInfo[0];
                $tabData = $tabConfigInfo[1];

                // Create file config
                $objFileParser->setSource(
                    $strRootDirPath . $strPathFile . $objFileParser->getStrFileExt(),
                    $tabData
                );
            }
        }
    }



    /**
     * Register specified module.
     *
     * @param AppInterface $objApp
     * @param string $strRootDirPath
     * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @param boolean $boolRegister = true
     * @throws RootDirPathInvalidFormatException
     */
    public static function register(
        AppInterface $objApp,
        $strRootDirPath,
        ParserBuilderInterface $objConfigParserBuilder = null,
        $boolRegister = true
    )
    {
        // Init var
        $strRootDirFullPath = $objApp->getStrRootDirPath() . $strRootDirPath;
        $boolRegister = (is_bool($boolRegister) ? $boolRegister : true);
        $objConfigParserBuilder = (is_null($objConfigParserBuilder) ? $objApp->getObjConfigParserBuilder() : $objConfigParserBuilder);
        $tabConfigParserType = (is_null($objConfigParserBuilder) ? null : $objConfigParserBuilder->getTabConfig());
        $objAppConfigParserBuilder = $objApp->getObjConfigParserBuilder();
        $objAppFileParser = $objAppConfigParserBuilder->getObjFileParser();

        // Init module configuration
        $tabConfig = array(
            ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH => $strRootDirPath
        );
        if(!is_null($tabConfigParserType))
        {
            $tabConfig[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER] = $tabConfigParserType;
        }

        // Set check argument
        RootDirPathInvalidFormatException::setCheck($strRootDirFullPath);

        // Run each module configurations
        $strAppModuleFilePath =
            $objApp->getStrRootDirPath() .
            ConstModuleBuilder::CONF_PATH_FILE_CONFIG_MODULE .
            $objAppFileParser->getStrFileExt();
        $tabData = $objAppFileParser->getData($strAppModuleFilePath);
        $tabData = $tabData[ConstModuleBuilder::TAB_CONFIG_MODULE_KEY_LIST];
        $tabModule = array();
        $boolFind = false;
        foreach($tabData as $tabModuleConfig)
        {
            $boolFind = (
                isset($tabModuleConfig[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH]) &&
                is_string($tabModuleConfig[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH]) &&
                ($tabModuleConfig[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH] == $strRootDirPath)
            );

            // Register module configuration
            if(!$boolFind)
            {
                $tabModule[] = $tabModuleConfig;
            }
            // Update module configuration, if required
            else if($boolRegister)
            {
                $tabModule[] = $tabConfig;
            }
        }

        // Add module configuration, if required
        if((!$boolFind) && $boolRegister)
        {
            $tabModule[] = $tabConfig;
        }

        // Save module configurations
        $tabData = array(
            ConstModuleBuilder::TAB_CONFIG_MODULE_KEY_LIST => $tabModule
        );
        $objAppFileParser->setSource(
            $strAppModuleFilePath,
            $tabData
        );
    }



    /**
     * Register list of modules.
     *
     * @param AppInterface $objApp
     * @param string $strRootDirPath
     * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @param boolean $boolRegister = true
     * @throws RootDirPathMultiInvalidFormatException
     * @throws RootDirPathInvalidFormatException
     */
    public static function registerMulti(
        AppInterface $objApp,
        $strRootDirPath,
        ParserBuilderInterface $objConfigParserBuilder = null,
        $boolRegister = true
    )
    {
        // Init var
        $strRootDirFullPath = $objApp->getStrRootDirPath() . $strRootDirPath;

        // Set check argument
        RootDirPathMultiInvalidFormatException::setCheck($strRootDirFullPath);

        // Run all path
        $tabDirPath = glob($strRootDirFullPath . '/*');
        foreach($tabDirPath as $strDirPath)
        {
            // Register module, if required
            if(is_dir($strDirPath))
            {
                static::register(
                    $objApp,
                    $strRootDirPath . '/' . basename($strDirPath),
                    $objConfigParserBuilder,
                    $boolRegister
                );
            }
        }
    }



}