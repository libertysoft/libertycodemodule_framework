<?php
/**
 * Description :
 * This class allows to define cache controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\cache\controller\command;

use liberty_code_module\framework\controller\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code_module\framework\cache\library\ConstCache;



class CacheController extends DefaultController
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods action
    // ******************************************************************************

    /**
     * Action to get keys,
     * from application cache repository.
     *
     * @return DefaultResponse
     */
    public function actionGetKey()
    {
        // Init var
        $objApp = $this->objApp;

        // Get render
        $strRender = implode(
            PHP_EOL,
            $objApp
                ->getObjCacheRepo()
                ->getTabSearchKey()
        );

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Action to remove items,
     * from application cache repository.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRemove(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabKey = (
            (($key = $objFrontController->getOptValue(ConstCache::COMMAND_OPT_NAME_KEY, false)) !== false) ?
                array_map(
                    function($strKey) {return trim($strKey);},
                    explode(',', $key)
                ) :
                array()
        );
        $objCacheRepo = $this
            ->objApp
            ->getObjCacheRepo();

        // Remove specified items
        if(count($tabKey) > 0)
        {
            $objCacheRepo->removeTabItem($tabKey);
        }
        // Remove all items
        else
        {
            $objCacheRepo->removeItemAll();
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



}