<?php

use liberty_code_module\framework\application\controller\command\AppController;



return array(
    // Application routes
    // ******************************************************************************

    'app_get_root_dir_path' => [
        'source' => 'app:get:root-path',
        'call' => [
            'class_path_pattern' => AppController::class . ':actionGetRootDirPath'
        ],
        'description' => 'Get application root directory full path'
    ]
);