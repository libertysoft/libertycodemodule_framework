<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\app_mode\library;



class ConstAppMode
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'framework_app_mode';



	// Configuration command line arguments
	const COMMAND_OPT_NAME_KEY = 'key';
    const COMMAND_OPT_NAME_ALL = 'all';



    // Exception message constants
    const EXCEPT_MSG_KEY_NOT_FOUND = 'Following key "%1$s" not found in mode collection!';



}