<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/controller/DefaultController.php');

include($strRootPath . '/src/application/library/ConstApp.php');
include($strRootPath . '/src/application/controller/command/AppController.php');

include($strRootPath . '/src/app_mode/library/ConstAppMode.php');
include($strRootPath . '/src/app_mode/exception/KeyNotFoundException.php');
include($strRootPath . '/src/app_mode/controller/command/AppModeController.php');

include($strRootPath . '/src/cache/library/ConstCache.php');
include($strRootPath . '/src/cache/controller/command/CacheController.php');

include($strRootPath . '/src/module/library/ConstModule.php');
include($strRootPath . '/src/module/exception/KeyNotFoundException.php');
include($strRootPath . '/src/module/exception/RootDirPathAddInvalidFormatException.php');
include($strRootPath . '/src/module/exception/RootDirPathMultiInvalidFormatException.php');
include($strRootPath . '/src/module/library/ToolBoxModule.php');
include($strRootPath . '/src/module/controller/command/ModuleController.php');