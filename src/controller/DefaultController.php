<?php
/**
 * Description :
 * This class allows to define default controller class.
 * Can be consider is base of all controller type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\controller;

use liberty_code\controller\controller\model\DefaultController as BaseDefaultController;

use liberty_code\framework\application\api\AppInterface;



abstract class DefaultController extends BaseDefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Application
     * @var AppInterface
     */
    protected $objApp;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AppInterface $objApp
     */
    public function __construct(AppInterface $objApp)
    {
        // Init properties
        $this->objApp = $objApp;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }



}