<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\framework\module\library;



class ConstModule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'framework_module';



	// Configuration command line arguments
	const COMMAND_ARG_NAME_KEY = 'key';
    const COMMAND_ARG_NAME_ROOT_DIR_PATH = 'root-dir-path';
    const COMMAND_OPT_NAME_CONFIG = 'config';
    const COMMAND_OPT_NAME_CONFIG_TEMPLATE = 'template';
    const COMMAND_OPT_NAME_REGISTER = 'register';
    const COMMAND_OPT_NAME_UNREGISTER = 'unregister';
    const COMMAND_OPT_NAME_LIST = 'list';



    // Exception message constants
    const EXCEPT_MSG_KEY_NOT_FOUND = 'Following key "%1$s" not found in module collection!';
    const EXCEPT_MSG_ROOT_DIR_PATH_ADD_INVALID_FORMAT =
        'Following module root directory path "%1$s" invalid! The root path must be a valid string folder full path and not exists.';
    const EXCEPT_MSG_ROOT_DIR_PATH_MULTI_INVALID_FORMAT =
        'Following module root directory path "%1$s" invalid! The root path must be a valid string folder full path.';



}