<?php

use liberty_code_module\framework\module\library\ConstModule;
use liberty_code_module\framework\module\controller\command\ModuleController;



return array(
    // Module routes
    // ******************************************************************************

    'module_get_key' => [
        'source' => 'module:get:key',
        'call' => [
            'class_path_pattern' => ModuleController::class . ':actionGetKey'
        ],
        'description' => 'Get all module keys'
    ],

    'module_get' => [
        'source' => 'module:get',
        'call' => [
            'class_path_pattern' => ModuleController::class . ':actionGet'
        ],
        'description' => 'Get specified module',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstModule::COMMAND_ARG_NAME_KEY,
                'description' => 'Specified module key, to get',
                'type_value' => 'string'
            ]
        ]
    ],

    'module_create' => [
        'source' => 'module:create',
        'call' => [
            'class_path_pattern' => ModuleController::class . ':actionCreate'
        ],
        'description' => 'Create specified module',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstModule::COMMAND_ARG_NAME_KEY,
                'description' => 'Module key',
                'type_value' => 'string'
            ],
            [
                'type' => 'argument',
                'name' => ConstModule::COMMAND_ARG_NAME_ROOT_DIR_PATH,
                'description' => 'Module root directory full path, from application root directory.',
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstModule::COMMAND_OPT_NAME_CONFIG, 'c'],
                'description' => 'Module configuration parser builder type',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstModule::COMMAND_OPT_NAME_CONFIG_TEMPLATE, 't'],
                'description' => 'Require configuration templates, after creation',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ],
            [
                'type' => 'option',
                'name' => [ConstModule::COMMAND_OPT_NAME_REGISTER, 'r'],
                'description' => 'Require registration, after creation',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ],

    'module_register' => [
        'source' => 'module:register',
        'call' => [
            'class_path_pattern' => ModuleController::class . ':actionRegister'
        ],
        'description' => '(Un)register specified one or many modules, to be (un)used in project',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstModule::COMMAND_ARG_NAME_ROOT_DIR_PATH,
                'description' => 'Module root directory full path, for one or set of modules, from application root directory',
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstModule::COMMAND_OPT_NAME_CONFIG, 'c'],
                'description' => 'Module configuration parser builder type',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstModule::COMMAND_OPT_NAME_UNREGISTER, 'u'],
                'description' => 'Require unregistration, to be unused in project',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ],
            [
                'type' => 'option',
                'name' => [ConstModule::COMMAND_OPT_NAME_LIST, 'l'],
                'description' => 'Require root directory path as directory where each sub-folder is module',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ]
);