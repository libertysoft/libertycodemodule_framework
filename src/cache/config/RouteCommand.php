<?php

use liberty_code_module\framework\cache\library\ConstCache;
use liberty_code_module\framework\cache\controller\command\CacheController;



return array(
    // Cache routes
    // ******************************************************************************

    'cache_get_key' => [
        'source' => 'cache:get:key',
        'call' => [
            'class_path_pattern' => CacheController::class . ':actionGetKey'
        ],
        'description' => 'Get all keys, from cache'
    ],

    'cache_remove' => [
        'source' => 'cache:remove',
        'call' => [
            'class_path_pattern' => CacheController::class . ':actionRemove'
        ],
        'description' => 'Remove specified or all items, from cache',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstCache::COMMAND_OPT_NAME_KEY, 'k'],
                'description' => 'Specified comma separated list of keys, to remove',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ]
        ]
    ]
);